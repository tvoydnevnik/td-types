# TvoyDnevnik types

## About

This repository contains all common interfaces for frontend and backend types coherence.

These interfaces are then used to form data types on both sides of the application.

## How to use / extend

To make use of this repository, simply use Typescript's `implements` keyword when declaring a new class.
This will enforce a newly created class to at least have those common properties on them,
while allowing to be extended for the purpose of application service you're working on.

This usage pattern also forms the basic extension guideline:

1. Add new interface with all necessary properties to this repository.
2. Implement the created interface while creating new data structure in the application service.

Basic example with `IUser` and `UserEntity` (even though backend doesn't use entities anymore):

```ts
// this repository

// ...
import { Column } from "typeorm";

export interface IUser {
	id: number;
	username: string;
	email: string;
}

// somewhere in the API respository

/* user.entity.ts */

// ...

@Entity("users")
export class UserEntity implements IUser {
	@PrimaryGeneratedColumn("identity")
	id: number;

	@Column("varchar")
	username: string;

	@Column("varchar")
	email: string;

	// our custom property independent of IUser
	@Column("varchar")
	first_name: string;
}
```

All the new types should be exported in the `td.types.barrel.ts` file.
`Types` prop will be appended to global `TD` namespace and will allow developer to user their newly created interface
without extra imports.
