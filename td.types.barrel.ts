//
// All interface exports go below
//

// Entity interfaces
export * from "./interfaces/users/user.interface";
export * from "./interfaces/users/parent.interface";
export * from "./interfaces/users/student.interface";
export * from "./interfaces/users/teacher.interface";
export * from "./interfaces/school/timetable-entry.interface";
export * from "./interfaces/periods-of-time/quarter.interface";
export * from "./interfaces/periods-of-time/quarter-week.interface";
export * from "./interfaces/periods-of-time/school-year.interface";
export * from "./interfaces/periods-of-time/period-of-time.interface";
export * from "./interfaces/school/school.interface";
export * from "./interfaces/school/form.interface";
export * from "./interfaces/school/form-subgroup.interface";
export * from "./interfaces/school/lesson.interface";
export * from "./interfaces/registration-queue-entry.interface";
export * from "./interfaces/school/bells-schedule.interface";
export * from "./interfaces/contact.interface";
export * from "./interfaces/users/headmaster.interface";
export * from "./interfaces/school/diary.interface";
export * from "./interfaces/school/form-lesson.interface";
export * from "./interfaces/quarter-pagination.interface";
export * from "./interfaces/school/final-marks/quarterly-mark.interface";
export * from "./interfaces/school/final-marks/yearly-mark.interface";
export * from "./interfaces/school/signatures/weekly-signature.interface";
export * from "./interfaces/school/signatures/quarterly-signature.interface";
export * from "./interfaces/school/signatures/yearly-signature.interface";

// Response interfaces
export * from "./interfaces/responses/timetable-responses.interface";
export * from "./interfaces/responses/diary-responses.interface";
export * from "./interfaces/responses/final-marks-response.interface";

// Request interfaces
export * from "./interfaces/requests/diary-requests.interface";
export * from "./interfaces/requests/timetable-requests.interface";
export * from "./interfaces/requests/quarter-pagination-request.interface";
export * from "./interfaces/requests/final-marks-request.interface";

// Payload interfaces
export * from "./interfaces/payloads/bells-schedule.payload.interface";
export * from "./interfaces/payloads/diary.payload.interface";
export * from "./interfaces/payloads/timetable.payload.interface";
export * from "./interfaces/payloads/user-registration.payload.interface";
export * from "./interfaces/payloads/final-marks.payload.interface";
export * from "./interfaces/payloads/school-year-wizard-payload.interface";

// Server-client standardized interaction
export * from "./interfaces/api/http-response.interface";
export * from "./interfaces/api/session-data.interface";

// Utility types
export * from "./utility-types/deep-partial"
