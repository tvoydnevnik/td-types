export interface IHttpResponse<T> {
	/**
	 * Could be used by frontend to notify user of the request status.
	 * Fill it with either a success message or an error message describing possible solutions.
	 */
	message: string;

	/**
	 * {@link https://developer.mozilla.org/en-US/docs/Web/HTTP/Status [MDN]: HTTP response status codes}
	 */
	status: number;

	/**
	 * Property of generic type representing the response data itself.
	 */
	data?: T;
}
