import { IUser } from "../users/user.interface";

export interface SessionDeviceData {
	/**
	 * User's web client.
	 * E.g. Chrome 102
	 */
	client: string;
	/**
	 * User's device (often not completely accurate, as set by User-Agent).
	 * E.g. Samsung Galaxy G975F
	 */
	device: string;
	/**
	 * User's device OS.
	 * E.g. Android 12.0
	 */
	os: string;
}

export interface SessionData {
	id: string;
	passport: { user: IUser };
	device_info: SessionDeviceData;
}
