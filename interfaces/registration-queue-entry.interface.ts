import { IUser } from "./users/user.interface";

export interface IRegistrationQueueEntry {
	id: number;
	code: string;
	user?: IUser;
	user_id: number;
}
