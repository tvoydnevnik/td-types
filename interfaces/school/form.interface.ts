import { IFormSubgroup } from "./form-subgroup.interface";
import { IStudent } from "../users/student.interface";
import { ITeacher } from "../users/teacher.interface";

export interface IForm {
	id: number;
	grade: number;
	letter: string;
	/**
	 * When the form has graduated it becomes archived and is no longer updated nor can be changed.
	 */
	master?: ITeacher;
	master_id: number;
	graduated: boolean;
	students?: IStudent[];
	subgroups?: IFormSubgroup[];
}
