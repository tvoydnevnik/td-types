import { IFormSubgroup } from "./form-subgroup.interface";
import { ITeacher } from "../users/teacher.interface";
import { ILesson } from "./lesson.interface";
import { IForm } from "./form.interface";
import { ISchoolYear } from "../periods-of-time/school-year.interface";

export interface IFormLesson {
	id: number;
	lesson?: ILesson;
	lesson_id: number;
	form?: IForm;
	form_id: number;
	form_subgroup?: IFormSubgroup;
	form_subgroup_id?: number;
	teacher?: ITeacher;
	teacher_id: number;
	school_year?: ISchoolYear;
	school_year_id: number;
}
