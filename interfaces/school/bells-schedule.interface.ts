import { z } from "zod";

export const ISchedule = z.object({
	lesson_index: z.number(),
	start_time: z.date().or(z.string()),
	end_time: z.date().or(z.string()),
});

export type ISchedule = z.infer<typeof ISchedule>;

export interface IBellsSchedule {
	id: number;
	name: string;
	schedule: ISchedule[];
}
