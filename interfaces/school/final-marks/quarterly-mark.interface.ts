import { IQuarter } from "../../periods-of-time/quarter.interface";
import { IStudent } from "../../users/student.interface";
import { IFormLesson } from "../form-lesson.interface";
import { IUser } from "../../users/user.interface";

export interface IQuarterlyMark {
	id: number;
	student_id: number;
	student?: IStudent;
	quarter_id: number;
	quarter?: IQuarter;
	form_lesson_id: number;
	form_lesson?: IFormLesson;
	user_id?: number;
	user?: IUser;
	mark?: string;
}
