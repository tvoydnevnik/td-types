import { ISchoolYear } from "../../periods-of-time/school-year.interface";
import { IStudent } from "../../users/student.interface";
import { IFormLesson } from "../form-lesson.interface";
import { IUser } from "../../users/user.interface";

export interface IYearlyMark {
	id: number;
	student_id: number;
	student?: IStudent;
	school_year_id: number;
	school_year?: ISchoolYear;
	form_lesson_id: number;
	form_lesson?: IFormLesson;
	user_id?: number;
	user?: IUser;
	mark?: string;
}
