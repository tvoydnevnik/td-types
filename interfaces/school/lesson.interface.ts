import { IFormLesson } from "./form-lesson.interface";

export interface ILesson {
	id: number;
	name?: string;
	form_lessons?: IFormLesson[];
}
