import { IQuarter } from "../../periods-of-time/quarter.interface";
import { IStudent } from "../../users/student.interface";
import { IParent } from "../../users/parent.interface";

export interface IQuarterlySignature {
	id: number;
	quarter_id: number;
	quarter?: IQuarter;
	student_id: number;
	student?: IStudent;
	parent_id: number;
	parent?: IParent;
	created_at: Date;
}
