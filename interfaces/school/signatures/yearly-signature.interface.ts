import { ISchoolYear } from "../../periods-of-time/school-year.interface";
import { IStudent } from "../../users/student.interface";
import { IParent } from "../../users/parent.interface";

export interface IYearlySignature {
	id: number;
	school_year_id: number;
	school_year?: ISchoolYear;
	student_id: number;
	student?: IStudent;
	parent_id: number;
	parent?: IParent;
	created_at: Date;
}
