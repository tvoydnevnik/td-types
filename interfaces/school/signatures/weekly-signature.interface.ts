import { IQuarterWeek } from "../../periods-of-time/quarter-week.interface";
import { IStudent } from "../../users/student.interface";
import { IParent } from "../../users/parent.interface";

export interface IWeeklySignature {
	id: number;
	quarter_week_id: number;
	quarter_week?: IQuarterWeek;
	student_id: number;
	student?: IStudent;
	parent_id: number;
	parent?: IParent;
	created_at: Date;
}
