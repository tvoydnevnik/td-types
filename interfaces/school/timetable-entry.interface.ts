import { IQuarterWeek } from "../periods-of-time/quarter-week.interface";
import { IFormLesson } from "./form-lesson.interface";
import { IDiaryEntry } from "./diary.interface";
import { IForm } from "./form.interface";

export interface ITimetableEntry {
	id: number;
	form?: IForm;
	form_id: number;
	quarter_week?: IQuarterWeek;
	quarter_week_id: number;
	form_lesson?: IFormLesson;
	form_lesson_id: number;
	weekday: number;
	/**
	 * The index of the lesson relative to the beginning of the weekday.
	 * E.g. students have got 7 lessons on weekday 2 (which is Wednesday), index of 3 will mean the third lesson on the schedule.
	 */
	index: number;
	classroom: string;
	start_time?: string;
	end_time?: string;
	homework?: string;
	lesson_topic?: string;
	diary?: IDiaryEntry[];
}
