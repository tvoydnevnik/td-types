import { ITimetableEntry } from "./timetable-entry.interface";
import { IStudent } from "../users/student.interface";
import { IUser } from "../users/user.interface";

export interface IDiaryEntry {
	id: number;
	timetable_entry?: ITimetableEntry;
	timetable_entry_id: number;
	student?: IStudent;
	student_id: number;
	user?: IUser;
	user_id?: number;
	marks: string[];
	comment?: string;
	created_at: Date;
	updated_at: Date;
}
