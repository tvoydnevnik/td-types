import { IStudent } from "../users/student.interface";
import { IForm } from "./form.interface";
import { IFormLesson } from "./form-lesson.interface";

export interface IFormSubgroup {
	id: number;
	name: string;
	form?: IForm;
	form_id: number;
	students?: IStudent[];
	form_lessons?: IFormLesson[];
}
