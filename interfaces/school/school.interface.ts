import { IHeadmaster } from "../users/headmaster.interface";
import { IContact } from "../contact.interface";

export interface ISchool {
	id: number;
	name: string;
	address?: string;
	contacts?: IContact;
	headmaster?: IHeadmaster;
	headmaster_id?: number;
}
