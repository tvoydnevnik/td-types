import { IUser } from "./user.interface";

export interface IHeadmaster {
	id: number;
	user?: IUser;
	user_id: number;
}
