import { IUser } from "./user.interface";
import { IForm } from "../school/form.interface";
import { IFormLesson } from "../school/form-lesson.interface";

export interface ITeacher {
	id: number;
	user?: IUser;
	user_id: number;
	duty?: string;
	master_of?: IForm;
	form_lessons?: IFormLesson[];
}
