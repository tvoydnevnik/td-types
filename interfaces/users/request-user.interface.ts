import { UserRole } from "./user.interface";

export interface IRequestUser {
	id: number;
	role: UserRole;
}
