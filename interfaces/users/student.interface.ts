import { IFormSubgroup } from "../school/form-subgroup.interface";
import { IParent } from "./parent.interface";
import { IUser } from "./user.interface";
import { IForm } from "../school/form.interface";

export interface IStudent {
	id: number;
	user?: IUser;
	user_id: number;
	form?: IForm;
	form_id?: number;
	form_subgroups?: IFormSubgroup[];
	parents?: IParent[];
}
