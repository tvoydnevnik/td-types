import { IUser } from "./user.interface";
import { IStudent } from "./student.interface";

export interface IParent {
	id: number;
	user?: IUser;
	user_id: number;
	students?: IStudent[];
}
