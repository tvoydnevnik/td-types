import { IRegistrationQueueEntry } from "../registration-queue-entry.interface";
import { IContact } from "../contact.interface";

export interface IUser {
	id: number;
	username?: string;
	email?: string;
	first_name: string;
	last_name: string;
	patronymic?: string;
	role: UserRole;
	personal_info?: IUserPersonalInfo;
	contacts?: IContact;
	is_registered: boolean;
	created_at: Date;
	updated_at: Date;
	invitations?: IRegistrationQueueEntry[];
}

/**
 * Value-based roles enum. The higher the value of the role, the more privilege the possessor has.
 * @enum {number}
 */
export enum UserRole {
	/** Admins have all access throughout the application.
	 * This is a service role and grants virtually unlimited privilege. If some feature is externally accessible (exposed via an API) it can be accessed by users with this role.
	 * Use with caution and supply strong passwords for Admin accounts to prevent data breaches and overall chaos.
	 */
	ADMIN = 10,
	/** Headmasters have most privilege and access through the application.
	 * They can change school info, manage teachers, students and their parents and do everything teachers can.
	 */
	HEADMASTER = 8,
	/** Teachers have a reasonable amount of privilege to manage their students and workflow.
	 * They can grade students, assign home-tasks, edit forms they're mastering and edit timetables.
	 */
	TEACHER = 5,
	/** Parents are pretty much like students, but have slightly more write access throughout the application.
	 * They can sign diaries, quarters and school years as well as manage their children's profiles.
	 */
	PARENT = 3,
	/** Students have the most basic abilities to view their diary, timetable and other public data.
	 * They have almost no write access except for editing their own user profile, commenting and messaging with each other.
	 */
	STUDENT = 1,
}

export const GetRoleString = (r: number): string | undefined =>
	isNaN(r)
		? undefined
		: typeof UserRole[r] === "string"
		? UserRole[r].toLowerCase()
		: undefined;

export interface IUserPersonalInfo {
	about?: string;
	birthday?: string;
}
