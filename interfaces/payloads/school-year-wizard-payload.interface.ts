import { z } from "zod";

export const ISchoolYearPayload = z.object({
	start_date: z.coerce.date(),
	end_date: z.coerce.date(),
	active: z.boolean().optional(),
});

export const IQuarterPayload = z.object({
	name: z.string(),
	start_date: z.coerce.date(),
	end_date: z.coerce.date(),
});

export const ISchoolYearWizardPayload = z.object({
	school_year: ISchoolYearPayload,
	quarters: z.array(IQuarterPayload),
	supersede: z.boolean().optional(),
});

export type ISchoolYearPayload = z.infer<typeof ISchoolYearPayload>;
export type IQuarterPayload = z.infer<typeof IQuarterPayload>;
export type ISchoolYearWizardPayload = z.infer<typeof ISchoolYearWizardPayload>;

export const IEditQuarterPayload = z
	.object({
		id: z.number(),
	})
	.merge(IQuarterPayload);

export const IEditSchoolYearWizardPayload = ISchoolYearWizardPayload.merge(
	z.object({
		quarters: z.array(IEditQuarterPayload),
	}),
);

export type IEditSchoolYearPayload = z.infer<typeof ISchoolYearPayload>;
export type IEditQuarterPayload = z.infer<typeof IEditQuarterPayload>;
export type IEditSchoolYearWizardPayload = z.infer<
	typeof IEditSchoolYearWizardPayload
>;
