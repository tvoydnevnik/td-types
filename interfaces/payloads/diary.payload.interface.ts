import { z } from "zod";

export const IDiaryPayloadEntry = z.object({
	timetable_entry_id: z.number(),
	student_id: z.number(),
	marks: z.array(z.string()).optional(),
	comment: z.string().optional(),
});

export const IDiaryPayload = z.object({
	user_id: z.number().optional(),
	diary: z.array(IDiaryPayloadEntry),
});

export type IDiaryPayloadEntry = z.infer<typeof IDiaryPayloadEntry>;
export type IDiaryPayload = z.infer<typeof IDiaryPayload>;
