import { ISchedule } from "../school/bells-schedule.interface";
import { z } from "zod";

export const IBellsSchedulePayload = z.object({
	name: z.string(),
	schedule: z.array(ISchedule),
});

export type IBellsSchedulePayload = z.infer<typeof IBellsSchedulePayload>;
