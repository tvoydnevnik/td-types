import { z } from "zod";

export const IQuarterlyMarksPayloadEntry = z.object({
	form_lesson_id: z.number(),
	student_id: z.number(),
	quarter_id: z.number(),
	mark: z.string().optional(),
});

export const IYearlyMarksPayloadEntry = z.object({
	form_lesson_id: z.number(),
	student_id: z.number(),
	school_year_id: z.number(),
	mark: z.string().optional(),
});

export const IFinalMarksPayload = z.object({
	user_id: z.number().optional(),
	quarterly_marks: z.array(IQuarterlyMarksPayloadEntry).optional(),
	yearly_marks: z.array(IYearlyMarksPayloadEntry).optional(),
});

export type IQuarterlyMarksPayloadEntry = z.infer<
	typeof IQuarterlyMarksPayloadEntry
>;

export type IYearlyMarksPayloadEntry = z.infer<typeof IYearlyMarksPayloadEntry>;

export type IFinalMarksPayload = z.infer<typeof IFinalMarksPayload>;
