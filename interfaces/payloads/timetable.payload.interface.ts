import { z } from "zod";

export const ITimetablePayloadEntry = z.object({
	lesson: z.number(),
	teacher: z.number(),
	form_subgroup: z.number().optional(),
	weekday: z.number(),
	index: z.number(),
	start_time: z.string(),
	end_time: z.string(),
	classroom: z.string(),
});

export const ITimetablePayload = z.object({
	form: z.number(),
	quarter: z.number(),
	payload: z.array(ITimetablePayloadEntry),
});

export type ITimetablePayloadEntry = z.infer<typeof ITimetablePayloadEntry>;
export type ITimetablePayload = z.infer<typeof ITimetablePayload>;
