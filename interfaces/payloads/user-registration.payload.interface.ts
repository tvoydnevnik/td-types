import { UserRole } from "../users/user.interface";
import { z } from "zod";

export const IUserRegistrationPayload = z.object({
	first_name: z.string(),
	last_name: z.string(),
	patronymic: z.string().optional(),
	role: z.nativeEnum(UserRole),
});

export type IUserRegistrationPayload = z.infer<typeof IUserRegistrationPayload>;
