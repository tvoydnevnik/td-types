export interface IContact {
	email?: string;
	phone?: string;
	telegram?: string;
	instagram?: string;
	/**
	 * Generic key-value pair.
	 * Example: `{ "LinkedIn": "Boris Matus" }`
	 */
	www?: Record<string, string>;
}
