import { IPeriodOfTime } from "../periods-of-time/period-of-time.interface";
import { z } from "zod";

export const ITimetableRequest = z.object({
	request_opts: z
		.object({
			with_diary: z.boolean().optional(),
			for_week: z.boolean().optional(),
			for_quarter: z.boolean().optional(),
			for_year: z.boolean().optional(),
			for_period: IPeriodOfTime.optional(),
		})
		.optional(),
	filtering_opts: z
		.object({
			user_id: z.number().optional(),
			form_id: z.number().optional(),
			teacher_id: z.number().optional(),
			lesson_id: z.number().optional(),
			form_lesson_id: z.number().optional(),
			subgroup_id: z.number().optional(),
			student_id: z.number().optional(),
			school_year_id: z.number().optional(),
			quarter_id: z.number().optional(),
			quarter_week_id: z.number().optional(),
			quarter_week_index: z.number().optional(),
		})
		.optional(),
});

export type ITimetableRequest = z.infer<typeof ITimetableRequest>;
