import { z } from "zod";

export const IQuarterPaginationRequest = z.object({
	quarter_id: z.number(),
});

export type IQuarterPaginationRequest = z.infer<
	typeof IQuarterPaginationRequest
>;
