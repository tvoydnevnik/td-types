import { z } from "zod";

export const IFinalMarksRequest = z.object({
	request_opts: z
		.object({
			for_year: z.boolean().optional(),
			for_quarter: z.boolean().optional(),
		})
		.optional(),
	filtering_opts: z
		.object({
			form_id: z.number().optional(),
			student_id: z.number().optional(),
			form_lesson_id: z.number().optional(),
			quarter_id: z.number().optional(),
			school_year_id: z.number().optional(),
		})
		.optional(),
});

export type IFinalMarksRequest = z.infer<typeof IFinalMarksRequest>;
