import { z } from "zod";

const IDiaryRequest = z.object({
	student_id: z.number().optional(),
	form_lesson_id: z.number().optional(),
	subgroup_id: z.number().optional(),
	quarter_id: z.number().optional(),
	school_year_id: z.number().optional(),
	quarter_week_id: z.number().optional(),
	quarter_week_index: z.number().optional(),
});

export type IDiaryRequest = z.infer<typeof IDiaryRequest>;
