export interface IQuarterPagination {
	current_week: number;
	first_week: number;
	last_week: number;
}
