import { IDiaryEntry } from "../school/diary.interface";

export interface IDiaryResponse {
	start_date: string;
	end_date: string;
	diary: IDiaryEntry[];
}
