import { ITimetableEntry } from "../school/timetable-entry.interface";

export interface ITimetableResponse {
	start_date: string;
	end_date: string;
	timetable: ITimetableEntry[];
}
