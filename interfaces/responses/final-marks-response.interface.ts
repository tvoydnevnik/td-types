import { IQuarterlyMark } from "../school/final-marks/quarterly-mark.interface";
import { IYearlyMark } from "../school/final-marks/yearly-mark.interface";
import { ISchoolYear } from "../periods-of-time/school-year.interface";

export interface IFinalMarksResponse {
	school_year: ISchoolYear;
	quarterly?: IQuarterlyMark[];
	yearly?: IYearlyMark[];
}
