import { z } from "zod";

export const IPeriodOfTime = z
	.object({
		start_date: z.coerce.date(),
		end_date: z.coerce.date(),
	})
	.refine((s) => s.start_date < s.end_date, {
		message: "Start date should be before the end date.",
		path: ["start_date", "end_date"],
	});

export type IPeriodOfTime = z.infer<typeof IPeriodOfTime>;
