import { IQuarterWeek } from "./quarter-week.interface";
import { ISchoolYear } from "./school-year.interface";

export interface IQuarter {
	id: number;
	name: string;
	school_year?: ISchoolYear;
	school_year_id: number;
	start_date: Date;
	end_date: Date;
	/** When pulling timetables and diaries backend determines the current school year, quarter and quarter week.
	 * Indexing dates indicate when to consider the quarter as current.
	 */
	indexing_start: Date;
	/** When pulling timetables and diaries backend determines the current school year, quarter and quarter week.
	 * Indexing dates indicate when to consider the quarter as current.
	 */
	indexing_end: Date;
	weeks?: IQuarterWeek[];
}
