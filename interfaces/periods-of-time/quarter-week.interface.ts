import { IQuarter } from "./quarter.interface";
import { ITimetableEntry } from "../school/timetable-entry.interface";

export interface IQuarterWeek {
	id: number;
	/** Index of the quarter week relatively to beginning of the quarter
	 * E.g. quarter has 12 weeks, index of 4 will mean the fourth week since the beginning of the quarter, the max index will be 12.
	 */
	index: number;
	quarter?: IQuarter;
	quarter_id: number;
	start_date: Date;
	end_date: Date;
	/** When pulling timetables and diaries backend determines the current school year, quarter and quarter week.
	 * Indexing dates indicate when to consider the week as current.
	 */
	indexing_start: Date;
	/** When pulling timetables and diaries backend determines the current school year, quarter and quarter week.
	 * Indexing dates indicate when to consider the week as current.
	 */
	indexing_end: Date;
	timetable?: ITimetableEntry[]
}
