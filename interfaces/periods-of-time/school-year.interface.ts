import { IQuarter } from "./quarter.interface";

export interface ISchoolYear {
	id: number;
	start_date: Date;
	end_date: Date;
	active?: boolean;
	quarters?: IQuarter[];
}
